# Copyright (C) 2019-2021 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import sys
import shutil
import json
import subprocess
import requests
import getpass
import pwd

from .base_stage import BaseStage

# On multi-core and/or multithreaded systems, take advantage of the extra
# power to compile Squid much more quickly.
CORECOUNT = os.cpu_count()
if (not CORECOUNT):
    CORECOUNT = 1

SQUID_UNAME = 'squid'

setup_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# This should really be part of the standard library:
def recursive_chown(dirname, uid, gid):
    for path, dirs, files in os.walk(dirname):
        for f in files:
            os.chown(os.path.join(path, f), uid, gid)
        for d in dirs:
            os.chown(os.path.join(path, d), uid, gid)

def download_file(url, dst):
    stream = requests.get(url, stream=True)
    with open(dst, 'wb') as dst_file:
        shutil.copyfileobj(stream.raw, dst_file)

class SquidStage(BaseStage):

    DEBIAN_DEPENDENCIES = ['libssl-dev', 'secure-delete',
                           'libnss3-tools', 'gcc', 'g++', 'make']

    DELETE_TARGETS = [
        '/usr/local/squid/',
        '/var/lib/ssl_db/',
        '/etc/systemd/system/squid.service',
        '/etc/init.d/squid'
    ]

    INSTALL_BLERB = '''Create squid user/group if they do not exist
Compile squid with SSL bumping enabled
Install squid and automatically start it at boot
Redirect all traffic from http/s ports to squid
Generate an ssl certificate for squid at /usr/local/squid/ssl_cert/myca.crt
    (Make sure this is trusted by your browser if you fail to connect securely)
Configure squid to block ad, facebook, and malicious hosts'''
    UNINSTALL_BLERB = 'Remove the squid background service'

    SQUID_UNAME = 'squid'

    # argparse does not have a nice way to store an argument independently
    # of the parser.
    @staticmethod
    def add_options(parser):
        parser.add_argument(
            '-a',
            '--admin',
            type=str,
            metavar='USERNAME',
            help="The admin account's username")

        # TODO: This blacklists option should accept a list of files for squid
        #       to interpret as dstdomain or dstdom_regex ACLs and block.
        '''parser.add_argument(
            '-b',
            '--blacklists',
            dest='user_blacklists',
            action='append',
            type=str,
            default=[],
            help=('The path to a custom squid blacklist ACL'
                  ' (Can be specified multiple times)'))'''

    # argparse does not have a nice way to store an argument independently
    # of the parser.
    @classmethod
    def add_actions(cls, parser):
        arg = parser.add_argument(
            '--update_acls',
            action='store_true',
            help=('Update the local Access Control Lists to block'
                  ' Ads/Facebook/Malicious hosts.'))

        return {arg.dest: cls.update_acls_interactive}


    @staticmethod
    def configure_squid_acls():

        # Empty files, because we want them to be admin-configurable:
        if (not os.path.isfile('/usr/local/squid/etc/blocked_domains.acl')):
            with open('/usr/local/squid/etc/blocked_domains.acl', 'w') as f:
                pass

        if (not os.path.isfile('/usr/local/squid/etc/allowed_domains.acl')):
            with open('/usr/local/squid/etc/allowed_domains.acl', 'w') as f:
                pass

        shutil.copy(setup_dir + '/squid/acls/squidbl-ads.acl',
                    '/usr/local/squid/etc/squidbl-ads.acl')

        shutil.copy(setup_dir + '/squid/acls/squidbl-facebook.acl',
                    '/usr/local/squid/etc/squidbl-facebook.acl')

        shutil.copy(setup_dir + '/squid/acls/squidbl-malicious.acl',
                    '/usr/local/squid/etc/squidbl-malicious.acl')

        shutil.copy(setup_dir + '/squid/acls/yoyo-ads.acl',
                    '/usr/local/squid/etc/yoyo-ads.acl')

    @classmethod
    def add_chromium_ca(cls, userdir, crt_path):

        # The regular non-snap database:
        db_dirs = ['{0}/.pki/nssdb'.format(userdir)]

        # The Chromium snap potential databases:
        chromium_snap_dir = '{0}/snap/chromium'.format(userdir)
        if (os.path.isdir(chromium_snap_dir)):
            db_dirs.extend([
                os.path.join(chromium_snap_dir, subdir, '.pki/nssdb')
                for subdir in os.listdir(chromium_snap_dir)
                if not os.path.islink(os.path.join(chromium_snap_dir, subdir))
            ])

        for db_dir in db_dirs:
            if (os.path.isdir(db_dir)):
                subprocess.run([
                    'certutil', '-d', 'sql:' + db_dir, '-A', '-t', 'C,,',
                    '-n', 'G3cko Industries local CA', '-i', crt_path
                ])

    @classmethod
    def configure_squid_ca(cls):

        ssl_dir = '/usr/local/squid/ssl_cert'
        ca_name = 'myca'
        key_path = '{0}/{1}.pem'.format(ssl_dir, ca_name)
        crt_path = '{0}/{1}.crt'.format(ssl_dir, ca_name)

        if (not os.path.isdir(ssl_dir)):
            os.mkdir(ssl_dir)

        generate_ssl_cert = ['openssl', 'req', '-new', '-newkey', 'rsa:2048',
                             '-days', '1365', '-nodes', '-x509',
                             '-keyout', key_path, '-out', crt_path]
        subprocess.run(generate_ssl_cert, check=True, universal_newlines=True,
                       input='\n'.join(['US', 'Texas', 'San Antonio',
                                        'G3cko Industries', 'ParentalWatch',
                                        '', 'G3ckoInd@gmail.com', '']))

        recursive_chown(ssl_dir, squid_uid, squid_gid)

        # The key, by default, has file permissions of 600, which prevents
        # the server from being easily restartable.
        os.chmod(key_path, 0o640)

        # Append myca.crt to myca.pem because squid wants both in the same file:
        with open(crt_path) as crt:
            with open(key_path, 'a') as key:
                key.write(crt.read())

        # Blanket-trust cert on some applications like apt, wget, and curl:
        shutil.copy(crt_path, '/usr/share/ca-certificates/')
        with open('/etc/ca-certificates.conf', 'a') as f:
            f.write(ca_name + '.crt\n')
        subprocess.run(['update-ca-certificates', '--fresh'], check=True)

        # Trust cert on Firefox:
        FF_POLICY_PATH = '/etc/firefox/policies/policies.json'
        os.makedirs(os.path.dirname(FF_POLICY_PATH), exist_ok=True)
        try:
            with open(FF_POLICY_PATH, 'r') as f:
                policies = json.load(f)
        except FileNotFoundError:
            policies = {}

        policies.setdefault('policies', {})
        policies['policies'].update({'Certificates': {'Install': [crt_path]}})
        with open(FF_POLICY_PATH, 'w') as f:
            json.dump(policies, f)

        # Blanket-trust cert on some applications like chromium:
        # This must be done per-user, unfortunately.
        user_str = subprocess.run(['getent', 'passwd'], check=True,
                                  stdout=subprocess.PIPE).stdout.decode()
        userdirs = []
        for line in user_str.split('\n'):
            if (line.strip() == ''):
                continue
            parts = line.split(':')

            # 65534 is the uid for "nobody"
            if (int(parts[2]) >= 1000 and int(parts[2]) != 65534):
                userdirs.append(parts[5])

        for userdir in userdirs:
            cls.add_chromium_ca(userdir, crt_path)

        # Setup the SSL DB for certificate caching
        ssl_db_dir = '/var/lib/ssl_db'
        init_ssl_db = ['/usr/local/squid/libexec/security_file_certgen',
                       '-c', '-s', ssl_db_dir, '-M', '4MB']
        subprocess.run(init_ssl_db, check=True)

        recursive_chown(ssl_db_dir, squid_uid, squid_gid)


    def install(self, args):
        print('Installing Squid...')

        global squid_uid
        global squid_gid

        try:
            squid_uid = pwd.getpwnam(SQUID_UNAME).pw_uid
            squid_gid = pwd.getpwnam(SQUID_UNAME).pw_gid
        except KeyError:
            # The squid user doesn't exist.
            subprocess.run(['useradd', SQUID_UNAME, '--system',
                            '--shell', '/usr/sbin/nologin',
                            '--home-dir', '/usr/local/squid'], check=True)
            squid_uid = pwd.getpwnam(SQUID_UNAME).pw_uid
            squid_gid = pwd.getpwnam(SQUID_UNAME).pw_gid

        # Add 'squid' as one of the admin's groups so sudo privileges are not
        # needed for normal use.
        # To authorize another user as an admin, simply add them to the group.
        if (not args.admin):
            admin_uname = os.getenv('SUDO_USER', getpass.getuser())
            print('No admin specified. Authorizing ' + admin_uname)
        else:
            admin_uname = args.admin
        subprocess.run(['usermod', '-a', '-G', str(squid_gid), admin_uname],
                       check=True)

        os.chdir('/tmp')

        # www.squid-cache.org does not have super reliable SSL...
        #squid_tar_dl = requests.get(
        #    'https://www.squid-cache.org/Versions/v4/squid-4.6.tar.gz',
        #    stream=True)
        #with open('squid-4.6.tar.gz', 'w') as squid_tar:
        #    shutil.copyfileobj(squid_tar_dl.raw, squid_tar)

        subprocess.run(['tar', '-zxvf',
                        setup_dir + '/squid/squid-4.13.tar.gz'],
                       check=True)

        os.chdir('squid-4.13')

        subprocess.run(['./configure', '--with-openssl', '--enable-ssl-crtd',
                        '--with-default-user=' + SQUID_UNAME], check=True)
        subprocess.run(['make', '-j' + str(CORECOUNT)],
                       check=True)
        subprocess.run(['make', 'install'])

        shutil.copy(setup_dir + '/squid/squid.conf',
                    '/usr/local/squid/etc/squid.conf')
        shutil.copy(setup_dir + '/squid/squid.service',
                    '/etc/systemd/system/squid.service')
        shutil.copy(setup_dir + '/squid/squid',
                    '/etc/init.d/squid')

        self.configure_squid_acls()

        self.configure_squid_ca()

        # Files we don't want to be readable by the child
        hidden_files = ['/var/logs/access.log', '/var/logs/cache.log',
                        '/etc/squid.conf', '/etc/blocked_domains.acl',
                        '/etc/allowed_domains.acl']

        os.chown('/usr/local/squid/var/logs', 0, squid_gid)
        os.chmod('/usr/local/squid/var/logs', 0o775)

        recursive_chown('/usr/local/squid/var/cache', squid_uid, squid_gid)
        recursive_chown('/usr/local/squid/var/run', squid_uid, squid_gid)

        for f in hidden_files:
            path = '/usr/local/squid' + f

            subprocess.run(['touch', path], check=True)
            os.chown(path, squid_uid, squid_gid)
            os.chmod(path, 0o660)

        subprocess.run(['systemctl', 'enable', 'squid'], check=True)


    def uninstall(self, args):

        result = subprocess.run(['systemctl', 'status', 'squid'],
                                input='q'.encode('utf-8'),
                                stdout=subprocess.DEVNULL)
        if (result.returncode == 0):
            # Squid is running.
            subprocess.run(['systemctl', 'stop', 'squid'], check=True)
            subprocess.run(['systemctl', 'disable', 'squid'], check=True)
        elif (os.path.isfile('/etc/systemd/system/squid.service')):
            # The squid service exists but is not running.
            subprocess.run(['systemctl', 'disable', 'squid'], check=True)


    # TODO: Check for duplicates in yoyo
    @classmethod
    def update_acls(cls, update_squid=False):

        download_file(
            'https://www.squidblacklist.org/downloads/squid-ads.acl',
            setup_dir + '/squid/acls/squidbl-ads.acl')

        download_file(
            'https://www.squidblacklist.org/downloads/squid-facebook.acl',
            setup_dir + '/squid/acls/squidbl-facebook.acl')

        download_file(
            'https://www.squidblacklist.org/downloads/squid-malicious.acl',
            setup_dir + '/squid/acls/squidbl-malicious.acl')

        yoyo_response = requests.get(
            'https://pgl.yoyo.org/adservers/serverlist.php?' +
            'hostformat=squid-dstdomain&mimetype=plaintext')

        # This file, unfortunately, comes wrapped in html headers, which Squid
        # can't interpret. It also neglects to filter subdomains. This code
        # strips off the html headers and re-adds subdomain filtering.
        with open(setup_dir + '/squid/acls/yoyo-ads.acl', 'w') as yoyo_ads:
            for line in yoyo_response.text.split('\n'):
                if (not line.strip().startswith('<') and
                    not line.strip().endswith('>')):
                    yoyo_ads.write('.' + line + '\n')

        if (update_squid):
            cls.configure_squid_acls()
            cls.test_squid_config()


    @classmethod
    def update_acls_interactive(cls, args):

        backup_files = ['squidbl-ads.acl', 'squidbl-facebook.acl',
                        'squidbl-malicious.acl', 'yoyo-ads.acl']

        squid_exists = os.path.isdir('/usr/local/squid/etc')
        if (squid_exists and not args.yes):
            answer = input("Update existing installation's ACLs? [y/N] ")
            update_squid = answer.strip().lower() == 'y'
        else:
            update_squid = squid_exists and args.yes

        if (update_squid):
            # Backup the existing files, just in case.
            if (not os.path.isdir(setup_dir + '/.backup')):
                os.mkdir(setup_dir + '/.backup')

            for f in backup_files:
                shutil.copy('/usr/local/squid/etc/' + f,
                            setup_dir + '/.backup/' + f)

        try:
            cls.update_acls(update_squid=update_squid)

            print()
            print('Success')
        except subprocess.CalledProcessError as e:

            if (not update_squid):
                raise

            # Undo everything!
            print('Squid config check failed. Undoing changes...')

            for f in backup_files:
                shutil.copy(setup_dir + '/.backup/' + f,
                            '/usr/local/squid/etc/' + f)

            raise


    @staticmethod
    def test_squid_config(args):
        subprocess.run(['/usr/local/squid/sbin/squid', '-k', 'parse'],
                       check=True)
