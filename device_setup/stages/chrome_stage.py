# Copyright (C) 2019-2020 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import json

from .base_stage import BaseStage

# Chromium/Chrome policy documentation:
#  http://www.chromium.org/administrators/policy-templates
CHROMIUM_POLICIES = {
    'IncognitoModeAvailability': 1,
    'ProxyMode': 'direct',
    'BuiltInDnsClientEnabled': False
}

policy_dirs = [
    os.path.join(d, 'policies/managed')
    for d in ['/etc/chromium', '/etc/chromium-browser', '/etc/opt/chrome']
]

# Disable incognito and proxy settings
# This does not require Chromium or Chrome to be installed.
class ChromeStage(BaseStage):

    DELETE_TARGETS = [os.path.join(d, 'parentalwatch.json')
                      for d in policy_dirs]

    INSTALL_BLERB = ('Disable incognito mode, proxy settings,'
                     ' and non-default DNS resolution in Chrome/Chromium')
    UNINSTALL_BLERB = None

    def install(self, args):
        print('Locking down Chrome/Chromium...')

        for d in policy_dirs:
            os.makedirs(d, exist_ok=True)
            with open(os.path.join(d, 'parentalwatch.json'), 'w') as policies:
                json.dump(CHROMIUM_POLICIES, policies)
