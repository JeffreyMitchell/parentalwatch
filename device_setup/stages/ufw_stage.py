# Copyright (C) 2019-2020 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import socket
import subprocess
import requests

from .base_stage import BaseStage

PROXY_PORTS = [641, 653, 1080, 4444, 4445, 5517, 7777, 8118, 8123, 8184,
               8388, 8580, 8997, 8998, 8999, 9001, 11214, 11215]

class UFWStage(BaseStage):

    DEBIAN_DEPENDENCIES = ['ufw']

    INSTALL_BLERB = ('Block certain common proxy ports\n'
                     'Allow port 22 (SSH) connections')
    UNINSTALL_BLERB = 'Re-allow connections on common proxy ports'

    def install(self, args):
        print('Configuring UFW...')

        # Allow incoming SSH connections
        subprocess.run(['ufw', 'allow', 'in', '22'], check=True)

        # Block ports commonly used by proxies
        for port in PROXY_PORTS:
            # 'reject' is easier to test (in test_ufw()) than 'deny' is,
            # though 'deny' may be better otherwise.
            subprocess.run(['ufw', 'reject', 'out', str(port)], check=True)

        # If running over SSH, UFW will give a y/n prompt stating that running
        # ssh connections may be disrupted. This is irrelevant because we've
        # already allowed SSH explicitly. --force disables the prompt.
        subprocess.run(['ufw', '--force', 'enable'], check=True)

    def uninstall(self, args):
        for port in PROXY_PORTS:
            subprocess.run(['ufw', 'delete', 'reject', 'out', str(port)],
                           check=True)

    @staticmethod
    def test_ufw(args):

        # UFW's 'reject' setting is easier to test for. If we can connect on a
        #   known-open port, but get ConnectionRefusedError on a blocked port,
        #   everything is working properly.
        requests.get('https://duckduckgo.com')

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sock.connect(('8.8.8.8', 641))
        except ConnectionRefusedError as e:
            assert e.errno == 111
        else:
            raise AssertionError("Expected error didn't appear while testing UFW.")
