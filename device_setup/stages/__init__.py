# Copyright (C) 2019-2020 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import importlib
import pkgutil

from .base_stage import BaseStage

pkg_dir = os.path.dirname(os.path.abspath(__file__))
for (module_loader, name, ispkg) in pkgutil.iter_modules([pkg_dir]):
    importlib.import_module('.' + name, __package__)
