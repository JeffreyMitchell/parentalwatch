# Copyright (C) 2019-2020 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import subprocess
import requests

from .base_stage import BaseStage

# Define OpenDNS as the only default DNS servers, so that known-bad sites
# can be filtered.
class OpenDNSStage(BaseStage):

    INSTALL_BLERB = 'Configure OpenDNS'
    UNINSTALL_BLERB = "Remove the 'immutable' option from /etc/resolv.conf"

    def install(self, args):
        print('Configuring OpenDNS...')

        if (os.path.islink('/etc/resolv.conf')):
            os.remove('/etc/resolv.conf')

        try:
            with open('/etc/resolv.conf', 'w') as resolv:
                resolv.write('nameserver 208.67.222.222\n')
                resolv.write('nameserver 208.67.220.220\n')
        except PermissionError:
            print(('Could not edit /etc/resolv.conf.'
                   ' Probably because setup has run before.'))
        else:
            # '+i' adds the 'immutable' attribute
            subprocess.run(['chattr', '+i', '/etc/resolv.conf'], check=True)

    def uninstall(self, args):
        resolv_conf = '/etc/resolv.conf'
        if (not os.path.islink(resolv_conf) and os.path.isfile(resolv_conf)):
            subprocess.run(['chattr', '-i', resolv_conf], check=True)

    @staticmethod
    def test_dns_filtering(args):

        # For some reason, instead of using a real redirect, OpenDNS sends
        # an html-wrapped JS script that sends you to either
        # block.opendns.com or phish.opendns.com.
        response = requests.get('http://www.internetbadguys.com')
        assert response.status_code == 403
