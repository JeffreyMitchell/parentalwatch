# Copyright (C) 2019-2020 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

from .base_stage import BaseStage

CRON_UPDATE_LINE = (
    '0 12\t* * *\troot\t'
    'apt-get update 2>&1 > /var/log/cron_update.log && '
    'DEBIAN_FRONTEND=noninteractive '
    'apt-get upgrade -y 2>&1 > /var/log/cron_upgrade.log\n')
CRON_POWEROFF_LINE = '0 2\t* * *\troot\tpoweroff\n'

# Setup auto-update and auto-poweroff
class CronStage(BaseStage):

    DEBIAN_DEPENDENCIES = ['cron']

    INSTALL_BLERB = ('Setup cron jobs to automatically install updates'
                     ' and poweroff the computer')
    UNINSTALL_BLERB = ('Remove the auto-update and auto-poweroff'
                       ' lines from /etc/crontab')

    def install(self, args):
        print('Configuring cron jobs...')

        with open('/etc/crontab', 'a') as crontab:
            crontab.write('\n' + CRON_UPDATE_LINE + CRON_POWEROFF_LINE)

    def uninstall(self, args):
        with open('/etc/crontab', 'r') as crontab:
            crontab_lines = crontab.readlines()

        with open('/etc/crontab', 'w') as crontab:
            for line in crontab_lines:
                #if ('poweroff' not in line and 'apt-get upgrade' not in line):
                if (line != CRON_UPDATE_LINE and line != CRON_POWEROFF_LINE):
                    crontab.write(line)

    @staticmethod
    def test_autoupdate(args):

        with open('/etc/crontab', 'r') as crontab:
            lines = list(crontab)

        if (CRON_UPDATE_LINE not in lines):
            print("Couldn't find autoupdate line in /etc/crontab")
            assert CRON_UPDATE_LINE in lines

    @staticmethod
    def test_autopoweroff(args):

        with open('/etc/crontab', 'r') as crontab:
            lines = list(crontab)

        if (CRON_POWEROFF_LINE not in lines):
            print("Couldn't find poweroff line in /etc/crontab")
            assert CRON_POWEROFF_LINE in lines
