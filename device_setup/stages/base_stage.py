# Copyright (C) 2019-2021 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

# Test functions should be written in pytest format (def test_xyz())
class BaseStage():

    # Simply a list of package dependencies on debian-based systems. If
    # there is a dependency that is sometimes unavailable depending on the
    # distro or version, but there is an alternative package, use a nested
    # list in order of preference.
    DEBIAN_DEPENDENCIES = []

    # A list of files to delete outright when uninstalling. Listing them
    # here makes it easier to describe to the user what will happen.
    DELETE_TARGETS = []

    # As concise descriptions as possible of what will happen during
    # installation and uninstallation.
    # They will follow the text "Preparing to (un)install. This will:"
    INSTALL_BLERB = 'TODO: Write an install blerb'
    UNINSTALL_BLERB = 'TODO: Write an uninstall blerb'

    @staticmethod
    def add_options(parser):
        pass

    @classmethod
    def add_actions(cls, parser):
        return {}

    def install(self, args):
        pass

    def uninstall(self, args):
        pass
