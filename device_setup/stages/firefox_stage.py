# Copyright (C) 2019-2021 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import json

from .base_stage import BaseStage

# It would be really nice if Firefox loaded policies from all json files in the
# "policies" dir like Chrome/Chromium do. That way it could work like a typical
# "xyz.d" config directory and simplify deployment.
POLICY_PATH = '/etc/firefox/policies/policies.json'

# Firefox policy documentation:
#  https://support.mozilla.org/en-US/kb/customizing-firefox-using-policiesjson
FIREFOX_POLICIES = {
    'DisablePrivateBrowsing': 1,
    'DisableForgetButton': 1,
    'Proxy': {
        'Mode': 'none',
        'Locked': True
    },
    'DNSOverHTTPS': {
        'Enabled': False,
        'Locked': True
    }
}

# Disable incognito and proxy settings
# This does not require Firefox to be installed.
class FirefoxStage(BaseStage):

    INSTALL_BLERB = ('Disable private browsing, proxy settings,'
                     ' and non-default DNS resolution in Firefox')
    UNINSTALL_BLERB = ('Allow private browsing, proxy settings,'
                       ' and non-default DNS resolution in Firefox')

    def install(self, args):
        print('Locking down Firefox...')

        os.makedirs(os.path.dirname(POLICY_PATH), exist_ok=True)
        try:
            with open(POLICY_PATH, 'r') as f:
                policies = json.load(f)
        except FileNotFoundError:
            policies = {}

        policies.setdefault('policies', {})
        policies['policies'].update(FIREFOX_POLICIES)
        with open(POLICY_PATH, 'w') as f:
            json.dump(policies, f)

    def uninstall(self, args):

        with open(POLICY_PATH, 'r') as f:
            policies = json.load(f)

        for k in FIREFOX_POLICIES.keys():
            if (k in policies['policies']):
                del policies['policies'][k]

        with open(POLICY_PATH, 'w') as f:
            json.dump(policies, f)
