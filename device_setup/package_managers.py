import shutil
import subprocess

class PkgManager():

    # Is this particular package manager present on the system?
    @staticmethod
    def is_compatible():
        raise NotImplementedError

    # Available packages vary slightly between distros. This code finds what's
    # needed to satisfy each stage. Return a list of strings.
    def collect_dependencies(self, stages):
        raise NotImplementedError

    def install_packages(self, packages, assume_yes=False):
        raise NotImplementedError

class DebPkgManager(PkgManager):

    apt_get = 'apt-get'
    apt_cache = 'apt-cache'

    # These are needed for remote management
    base_deps = ['openssh-server', 'sudo']

    @classmethod
    def is_package_installable(cls, pkg):
        result = subprocess.run([cls.apt_cache, 'show', pkg],
                                stdout=subprocess.PIPE)
        return result.returncode == 0

    @classmethod
    def is_compatible(cls):
        return (shutil.which(cls.apt_get)
                and shutil.which(cls.apt_cache)) != None

    def collect_dependencies(self, stages):

        unchecked_deps = self.base_deps
        for stage in stages:
            unchecked_deps = unchecked_deps + stage.DEBIAN_DEPENDENCIES

        subprocess.run([self.apt_get, 'update'],
                       stdout=subprocess.DEVNULL,
                       check=True)

        deps = []
        for dep in unchecked_deps:
            if (type(dep) == str):

                if (dep in deps):
                    continue

                assert self.is_package_installable(dep)
                deps.append(dep)

            elif (type(dep) == list):

                found_dependency = False
                for pkg in dep:

                    if (pkg in deps):
                        found_dependency = True
                        break

                    if (self.is_package_installable(pkg)):
                        # Found a package to satisfy the dependency.
                        deps.append(pkg)
                        found_dependency = True
                        break

                assert found_dependency

        return deps

    def install_packages(self, pkgs, assume_yes=False):

        apt_install_cmd = [self.apt_get, 'install'] + pkgs

        if (assume_yes):
            apt_install_cmd.append('-y')

        subprocess.run(apt_install_cmd, check=True)
