#!/usr/bin/python3

# Copyright (C) 2019-2020 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import shutil
import argparse

from stages import BaseStage
from package_managers import PkgManager


def test_install(args):
    print('Testing installation...')

    try:

        # Run every function with the "test_" prefix
        for StageClass in BaseStage.__subclasses__():
            for func_name in dir(StageClass):
                if (func_name.startswith('test_')):
                    getattr(StageClass, func_name)(args)

    except Exception as e:
        print('Installation check failed:')
        raise

    print()
    print('Installation check passed.')


def install(args):

    loaded_stages = BaseStage.__subclasses__()

    package_manager = None
    for P in PkgManager.__subclasses__():
        if (P.is_compatible()):
            package_manager = P()
            break

    if (not package_manager):
        raise Exception('Could not find supported package manager.')

    print('Checking dependencies...')
    deps = package_manager.collect_dependencies(loaded_stages)

    # This description always needs to be accurate.
    text = ('''Preparing to install. This will:
Ensure the following packages are installed:
    {0}
'''.format('\n    '.join(deps)))

    text = text + '\n'.join([s.INSTALL_BLERB
                             for s in loaded_stages
                             if s.INSTALL_BLERB])

    print(text)

    if (not args.yes):
        question = '\n\nDo you want to continue? [y/n] '
        if (input(question).strip().lower() != 'y'):
            return

    package_manager.install_packages(deps, args.yes)

    for Stage in loaded_stages:
        Stage().install(args)

    test_install(args)

    print()
    print('Setup completed successfully. Please reboot to apply changes.')


def uninstall(args):

    loaded_stages = BaseStage.__subclasses__()

    delete_targets = []
    for stage in loaded_stages:
        delete_targets = delete_targets + stage.DELETE_TARGETS

    # This description always needs to be accurate.
    text = ('''Preparing to uninstall. This will:
Delete the following files:
    {0}
'''.format('\n    '.join(delete_targets)))

    text = text + '\n'.join([s.UNINSTALL_BLERB
                             for s in loaded_stages
                             if s.UNINSTALL_BLERB])

    print(text)

    if (not args.yes):
        question = '\n\nDo you want to continue? [y/n] '
        if (input(question).strip().lower() != 'y'):
            return

    for stage in loaded_stages:
        stage().uninstall(args)

    for f in delete_targets:
        if (os.path.isdir(f)):
            shutil.rmtree(f)
        elif (os.path.isfile(f)):
            os.remove(f)

    print()
    print('Done. Please reboot for changes to take full effect.')


def parse_args(stages):

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument(
        '-y',
        '--yes',
        '--assume-yes',
        action='store_true')

    # Collect options from stages:
    for stage in stages:
        stage.add_options(parser)

    # add_mutually_exclusive_group() doesn't yet support title and description:
    workaround_group = parser.add_argument_group(
        'actions',
        ('Specify one of these to perform a different action\n'
         '(Default action is --install)'))
    action_group = workaround_group.add_mutually_exclusive_group()
    actions = {}

    arg = action_group.add_argument(
        '--install',
        action='store_true',
        help='Install ParentalWatch components on this computer.')
    actions[arg.dest] = install

    arg = action_group.add_argument(
        '--test',
        action='store_true',
        help='Test the current installation')
    actions[arg.dest] = test_install

    arg = action_group.add_argument(
        '--uninstall',
        action='store_true',
        help='Remove the current installation')
    actions[arg.dest] = uninstall

    # Collect actions from stages:
    for stage in stages:

        stage_actions = stage.add_actions(action_group)
        for action in stage_actions:
            assert action not in actions
            actions[action] = stage_actions[action]

    return (parser.parse_args(), actions)


if (__name__ == '__main__'):

    args, actions = parse_args(BaseStage.__subclasses__())

    default = True
    for attr in actions:
        if (getattr(args, attr)):
            actions[attr](args)
            default = False
            break

    if (default):
        install(args)
