# Copyright (C) 2019 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os

base_path = os.path.dirname(os.path.abspath(__file__))

_blocks = {}

def get(name):

    if (name not in _blocks):
        with open('{0}/html_blocks/{1}.html'.format(base_path, name)) as f:
            _blocks[name] = f.read()

    return _blocks[name]
