#!/usr/bin/python3

# Copyright (C) 2019 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import json
import fileinput
from urllib3.util import parse_url

from parentalwatch.util import load_default_filters

# This function has a lot of performance optimizations.
# Some of the code will look somewhat counterintuitive.
def filter_line(log, filters):

    if (log['timestamp'] < filters['start_time']
        or log['timestamp'] > filters['end_time']):
        return False

    # This once caused the wrong link to be filtered:
    #if ('_ABORTED' in log['squid_result_code']):
    #    return False

    if (log['http_status_code'] in filters['status_codes']):
        return False

    if (log['response_type'] != 'text/html'
        and (log['response_size'] < filters['size_threshold']
             or log['response_type'] in filters['types'])):
        return False

    # There are two size checks because one only applies if type != html.
    # hard_size_cutoff should be extremely small so it only catches errors
    # like one which has a size of 0 yet a type of text/html
    if (log['response_size'] < filters['hard_size_cutoff']):
        return False

    # Parse the URL
    try:
        url_components = parse_url(log['url'])
    except:
        print('Warning: Could not parse URL "{0}"'.format(log['url']))
        return False

    # Sometimes, parse_url() doesn't throw an exception when it fails.
    # (Observed on urllib3==1.25.3.) ¯\_(ツ)_/¯
    if (not url_components.host):
        return False

    current_page = url_components.host
    if (url_components.port != None):
        current_page = '{0}:{1}'.format(current_page, url_components.port)
    if (url_components.path != None):
        current_page = current_page + url_components.path
    if (current_page[-1] == '/'):
        current_page = current_page[:-1]

    if (current_page + '/' in filters['pages']
        or current_page.startswith(filters['tuple_pages'])):
        return False

    if ('.' + url_components.host in filters['hosts']
        or url_components.host.endswith(filters['tuple_hosts'])):
        return False

    return True

def process_file(f, filters):

    '''
    Format:
    {
        "host": {
            "/blah/?": {
                "timestamps": [
                    15151515,
                    15151530
                ],
                "protocol": "https",
                "type": "image/jpeg",
                "http_status": 200
            },
        },
    }
    '''
    urls = {}

    for line in f:

        words = line.split()

        # There are 10 different fields in the logs
        log_entry = {
            'timestamp': float(words[0]),
            #'time_elapsed': words[1], # This is not useful.
            #'local_addr': words[2], # This is not useful.
            #'result_status': words[3],
            'squid_result_code': words[3].split('/')[0],
            'http_status_code': int(words[3].split('/')[1]),
            'response_size': int(words[4]),
            'http_method': words[5],
            'url': ' '.join(words[6:-3]),
            'response_type': words[-1],
            #'remote_addr': words[-2], # This is probably not useful.
            #'unknown': words[-3]
        }

        if (not filter_line(log_entry, filters)):
            continue

        url = parse_url(log_entry['url'])
        if (url.host not in urls):
            urls[url.host] = {}

        full_path = ''
        if (url.path):
            full_path = url.path
        if (url.query):
            full_path = full_path + '?' + url.query
        if (url.port):
            full_path = ':' + str(url.port) + full_path

        if (full_path not in urls[url.host]):
            urls[url.host][full_path] = {'timestamps': []}

        urls[url.host][full_path]['protocol'] = url.scheme
        urls[url.host][full_path]['type'] = log_entry['response_type']
        urls[url.host][full_path]['http_status'] = log_entry['http_status_code']
        urls[url.host][full_path]['size'] = log_entry['response_size']
        urls[url.host][full_path]['timestamps'].append(log_entry['timestamp'])

    return urls


if (__name__ == '__main__'):
    filters = load_default_filters()
    print(json.dumps(process_file(fileinput.input(), filters), indent=4, sort_keys=True))
