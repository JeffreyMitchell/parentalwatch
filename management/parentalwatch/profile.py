# Copyright (C) 2019 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import shutil
import time
import subprocess
import json

from parentalwatch import html_blocks
from parentalwatch.ssh_tool import ssh_tool
from parentalwatch.squid_log_filter import process_file
from parentalwatch.util import join_url_dicts, split_url_dict, time_string
from parentalwatch.util import write_html_url_dict

DEFAULT_BLOCKED_HOSTS = ['bfmio.com', 'districtm.io', 'gleam.io',
                         'twitter.com', 'aaxdetect.com', 'lemonade.com',
                         'clicktale.com', 'clicktale.net']
DEFAULT_LOG_PATH = '/usr/local/squid/var/logs/access.log'

MEDIA_TYPES = ['JPG', 'PNG', 'MPEG', 'GIF']

class Profile:

    def __init__(self, name, admin, base_path='.'):
        self.name = name
        self.admin_account = admin
        self.devices = []
        self.banned_sites = DEFAULT_BLOCKED_HOSTS
        self.squid_log_path = DEFAULT_LOG_PATH
        self.path = None
        self.base_path = base_path

    @classmethod
    def load(cls, path, base_path='.'):

        name = os.path.splitext(os.path.basename(path))[0]

        with open(path) as f:
            profile_dict = json.load(f)

        profile = cls(name,
                      profile_dict['admin_account'],
                      base_path=base_path)

        profile.devices = profile_dict['devices']
        profile.banned_sites = profile_dict['banned_sites']
        profile.squid_log_path = profile_dict['squid_log_path']
        profile.path = path

        return profile

    def reload(self):
        # If this is ever needed, it should reload from the file
        # and error out if there is none.
        pass

    def save(self, path=None):

        if (path):
            save_path = path
            self.path = path
        else:
            save_path = self.path

        if (not save_path):
            raise ValueError('No path was given to save() and'
                             ' the profile has no stored path.')

        profile_dict = {
            'admin_account': self.admin_account,
            'devices': self.devices,
            'banned_sites': self.banned_sites,
            'squid_log_path': self.squid_log_path
        }

        with open(save_path, 'w') as f:
            json.dump(profile_dict, f, indent=4, sort_keys=True)

    def get_ssh_key(self):

        if (not self.path):
            return None

        ssh_path = '{0}/{1}.pem'.format(os.path.dirname(self.path), self.name)
        if (os.path.isfile(ssh_path)):
            return ssh_path

        return None

    # query can be either an integer ID or the name of the device.
    def get_device(self, query):

        try:
            device = self.devices[int(query)]
        except ValueError:
            device = None

            for dev in self.devices:
                if (dev['name'] == query):
                    device = dev

        if (device == None):
            raise IndexError

        return device

    # This will SSH into each configured device and
    # change its banned hosts and ssh keys
    def update_remote_configs(self):

        tmp_path = '/tmp/{0}_banned_hosts.acl'.format(self.name)

        if (not os.path.isdir(os.path.dirname(tmp_path))):
            tmp_path = '.{0}_banned_hosts.acl'.format(self.name)
        
        with open(tmp_path, 'w') as f:
            for host in self.banned_sites:
                if (host[0] != '.'):
                    host = '.' + host

                f.write(host + '\n')

        succeeded_devices = []
        failed_devices = []

        # Iterate in reverse order so the most recent device, which
        # is the most likely one to be unconfigured, gets priority.
        for device in reversed(self.devices):

            try:
                ssh = ssh_tool(self.admin_account, device['address'],
                               self.get_ssh_key(),
                               '{0}/ssh_known_hosts'.format(self.base_path))

                if (ssh.key_path):
                    ssh.copy_key()

                ssh.scp_to(tmp_path,
                           '/usr/local/squid/etc/blocked_domains.acl')

                device['last_updated'] = time.time()

                succeeded_devices.insert(0, device['name'])
            except subprocess.CalledProcessError:
                failed_devices.insert(0, device['name'])

        os.remove(tmp_path)

        return (succeeded_devices, failed_devices)

    def remove_key_remotely(self, key_path=None, ip_addresses=None):

        if (key_path == None):
            key_path = self.get_ssh_key()

        assert key_path != None

        if (ip_addresses == None):
            ip_addresses = [d['address'] for d in self.devices]

        # key_path points to a private key. We need a public one.
        public_key = subprocess.run(['ssh-keygen', '-y', '-f', key_path],
                                    stdout=subprocess.PIPE,
                                    check=True).stdout.decode().strip()

        for device in ip_addresses:

            try:
                ssh = ssh_tool(self.admin_account, device, self.get_ssh_key(),
                               '{0}/ssh_known_hosts'.format(self.base_path))
                ssh.ssh("sed -i '\\\\{0}\\d' ~/.ssh/authorized_keys".format(
                    public_key))
            except subprocess.CalledProcessError:
                pass

    def retrieve_logs(self):

        if (not self.devices):
            raise AssertionError('No devices to retrieve data from.')

        log_dir = '{0}/latest_logs/{1}'.format(self.base_path, self.name)

        # Delete the old ones in case a hostname or IP address changed:
        if (os.path.exists(log_dir)):
            shutil.rmtree(log_dir)

        os.makedirs(log_dir)

        succeeded_devices = []
        failed_devices = []

        for device in self.devices:

            try:
                ssh = ssh_tool(self.admin_account, device['address'],
                               self.get_ssh_key(),
                               '{0}/ssh_known_hosts'.format(self.base_path))

                dst_path = os.path.join(log_dir, device['name'] + '.log')

                ssh.scp_from(self.squid_log_path, dst_path)

                succeeded_devices.append(device['name'])
            except subprocess.CalledProcessError:
                failed_devices.append(device['name'])

        if (not succeeded_devices):
            raise AssertionError('Could not connect to any devices.')

        return (succeeded_devices, failed_devices)

    def get_history(self, filters=None):

        if (not filters):
            filters = {
                'hosts': set(),
                'tuple_hosts': (),
                'pages': set(),
                'tuple_pages': (),
                'size_threshold': 0,
                'hard_size_cutoff': 0,
                'status_codes': set(),
                'types': set()
            }

        filters.setdefault('start_time', 0)
        filters.setdefault('end_time', time.time())

        url_dicts = []
        used_devices = []
        unused_devices = [dev['name'] for dev in self.devices]

        log_dir = '{0}/latest_logs/{1}'.format(self.base_path, self.name)
        if (os.path.exists(log_dir)):
            for log_name in os.listdir(log_dir):

                # Log name format will be device_name.log
                used_devices.append(log_name[:-4])
                unused_devices.remove(log_name[:-4])

                with open(os.path.join(log_dir, log_name)) as f:
                    url_dicts.append(process_file(f, filters))

        if (len(url_dicts) == 0):
            raise AssertionError('There are no log files to process.')

        urls = join_url_dicts(url_dicts)

        return urls, used_devices, unused_devices

    def write_urls_to_file(self, urls, used_devices, unused_devices, output_path):

        # This is ugly, but it gets the latest timestamp value.
        try:
            latest_timestamp = max([
                max([
                    max(path['timestamps'])
                    for path in paths.values()
                ])
                for paths in urls.values()
            ])
        except ValueError:
            latest_timestamp = 0

        # Also very ugly, but it gets the earliest timestamp value.
        try:
            earliest_timestamp = min([
                min([
                    min(path['timestamps'])
                    for path in paths.values()
                ])
                for paths in urls.values()
            ])
        except ValueError:
            earliest_timestamp = 0

        blocked_urls = split_url_dict(urls, lambda x: x['http_status'] == 403)

        head_text = html_blocks.get('head')
        host_text = html_blocks.get('host')

        head_text = head_text.replace('$NAME', self.name)
        head_text = head_text.replace('$EARLIEST', time_string(earliest_timestamp))
        head_text = head_text.replace('$LATEST', time_string(latest_timestamp))
        head_text = head_text.replace('$USED_DEVICES', ', '.join(used_devices))
        head_text = head_text.replace('$UNUSED_DEVICES', ', '.join(unused_devices))

        if (len(unused_devices) == 0):
            head_text = head_text.replace('$HIDDEN_WARNING', 'hidden="true"')
        else:
            head_text = head_text.replace('$HIDDEN_WARNING', '')

        with open(output_path, 'w') as history_page:
            history_page.write(head_text)

            write_html_url_dict(history_page, urls)

            history_page.write('</br>\n')
            history_page.write(host_text.replace(
                '$HOST', 'Blocked URLs (Not necessarily blocked by Squid)'))

            write_html_url_dict(history_page, blocked_urls)

            history_page.write('</details>')

    def compile_history(self, path, filters=None, include_images=True):

        urls, used_devices, unused_devices = self.get_history(filters=filters)

        if (not include_images):
            split_url_dict(urls, lambda x: (
                x['type'].startswith(('image', 'video', 'audio'))
                or x['type'] in MEDIA_TYPES))

        self.write_urls_to_file(urls, used_devices, unused_devices, path)

    def compile_media(self, path, filters=None):

        urls, used_devices, unused_devices = self.get_history(filters=filters)

        split_url_dict(urls, lambda x: not (
            x['type'].startswith(('image', 'video', 'audio'))
            or x['type'] in MEDIA_TYPES))

        self.write_urls_to_file(urls, used_devices, unused_devices, path)
