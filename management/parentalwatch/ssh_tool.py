# Copyright (C) 2019 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import sys
import shutil
import subprocess

# Default connection timeout is over 2 minutes, which is too long to
# wait for LAN connections.
CONNECTTIMEOUT='ConnectTimeout=20'

class ssh_tool:

    def __init__(self, username, address, key_path=None,
                 known_hosts_path=None):
        self.username = username
        self.address = address
        self.key_path = key_path
        self.known_hosts_path = known_hosts_path

    def ssh(self, command, interactive=False, check=True,
            StrictHostKeyChecking=False):

        ssh_args = ['ssh', '-o', CONNECTTIMEOUT]

        if (interactive):
            ssh_args.append('-t')

        if (self.key_path):
            ssh_args.extend(['-i', self.key_path])

        if (not StrictHostKeyChecking):
            ssh_args.extend(['-o', 'StrictHostKeyChecking=no'])

        if (self.known_hosts_path):
            ssh_args.extend(['-o',
                             'UserKnownHostsFile="{0}"'.format(
                                 self.known_hosts_path)])

        ssh_args.append(self.username + '@' + self.address)

        ssh_args.append(command)

        kwargs = {
            'check': check
        }

        if (not interactive):
            kwargs['stdout'] = subprocess.PIPE

        return subprocess.run(ssh_args, **kwargs)

    def scp_to(self, local_path, remote_path='', check=True,
               StrictHostKeyChecking=False):

        scp_args = ['scp', '-r', '-o', CONNECTTIMEOUT]

        if (self.key_path):
            scp_args.extend(['-i', self.key_path])

        if (not StrictHostKeyChecking):
            scp_args.extend(['-o', 'StrictHostKeyChecking=no'])

        if (self.known_hosts_path):
            if (os.name != 'nt' or ' ' not in self.known_hosts_path):
                scp_args.extend(['-o',
                                 'UserKnownHostsFile="{0}"'.format(
                                     self.known_hosts_path)])
            else:
                # Tested on a fully updated Windows 10 VM on 6/22/2019:
                print(('Not using "-o UserKnownHostsFile=..." because that'
                       ' option is broken in Windows scp when there are spaces'
                       ' in the file path.'), file=sys.stderr)

        scp_args.append(local_path)
        scp_args.append('{0}@{1}:{2}'.format(self.username,
                                             self.address,
                                             remote_path))

        return subprocess.run(scp_args, check=check)

    def scp_from(self, remote_path, local_path='.', check=True,
                 StrictHostKeyChecking=False):

        scp_args = ['scp', '-r', '-o', CONNECTTIMEOUT]

        if (self.key_path):
            scp_args.extend(['-i', self.key_path])

        if (not StrictHostKeyChecking):
            scp_args.extend(['-o', 'StrictHostKeyChecking=no'])

        if (self.known_hosts_path):
            if (os.name != 'nt' or ' ' not in self.known_hosts_path):
                scp_args.extend(['-o',
                                 'UserKnownHostsFile="{0}"'.format(
                                     self.known_hosts_path)])
            else:
                # Tested on a fully updated Windows 10 VM on 6/22/2019:
                print(('Not using "-o UserKnownHostsFile=..." because that'
                       ' option is broken in Windows scp when there are spaces'
                       ' in the file path.'), file=sys.stderr)

        scp_args.append('{0}@{1}:{2}'.format(self.username,
                                             self.address,
                                             remote_path))
        scp_args.append(local_path)

        return subprocess.run(scp_args, check=check)

    # TODO: Delete this as soon as Windows has ssh-copy-id.
    #       This should only ever be called by copy_key().
    def _tmp_ssh_copy_id_replacement_for_windows(self, check=True):

        if (not self.key_path):
            return

        # Try sshing without password prompting (Will immediately be denied
        # if key is not authorized.)
        ssh_args = ['ssh', '-o', CONNECTTIMEOUT, '-i', self.key_path,
                    '-o', 'StrictHostKeyChecking=no', '-o', 'BatchMode=yes']

        if (self.known_hosts_path):
            ssh_args.extend(['-o',
                             'UserKnownHostsFile="{0}"'.format(
                                 self.known_hosts_path)])

        ssh_args.extend(['{0}@{1}'.format(self.username, self.address),
                         'exit 0'])

        try:
            return subprocess.run(ssh_args, check=True)
        except subprocess.CalledProcessError as e:
            # SSH returns 255 if the key was denied, but also does so for other
            # connection-related errors.
            if (e.returncode != 255):
                raise

        pub_key = subprocess.run(['ssh-keygen', '-y', '-f', self.key_path],
                                 stdout=subprocess.PIPE,
                                 check=True).stdout.decode().strip()

        # Since SSH couldn't get in, append to the authorized_hosts file.
        ssh_args = ['ssh', '-o', CONNECTTIMEOUT,
                    '-o', 'StrictHostKeyChecking=no']

        if (self.known_hosts_path):
            ssh_args.extend(['-o',
                             'UserKnownHostsFile="{0}"'.format(
                                 self.known_hosts_path)])

        ssh_args.extend(['{0}@{1}'.format(self.username, self.address),
                         ('mkdir -p ~/.ssh && echo {0}'
                          ' >> ~/.ssh/authorized_keys').format(pub_key)])

        return subprocess.run(ssh_args, check=check)

    def copy_key(self, check=True, StrictHostKeyChecking=False):

        # While it's great that Windows finally has SSH, they're still missing
        # an important piece. Namely, ssh-copy-id, which provides an incredible
        # ammount of convenience with this tool.
        if (os.name == 'nt' and not shutil.which('ssh-copy-id')):
            print(('Using an awkward workaround because Windows doesn\'t have'
                   ' the "ssh-copy-id" command.'), file=sys.stderr)
            return self._tmp_ssh_copy_id_replacement_for_windows(check=check)

        ssh_args = ['ssh-copy-id', '-o', CONNECTTIMEOUT]

        if (self.key_path):
            ssh_args.extend(['-i', self.key_path])

            # Unfortunately, ssh-copy-id won't work with just a private key
            # and it won't generate its own public key.
            public_key = subprocess.run(['ssh-keygen', '-y', '-f',
                                         self.key_path],
                                        stdout=subprocess.PIPE,
                                        check=True).stdout.decode()
            with open(self.key_path + '.pub', 'w') as f:
                f.write(public_key)

        if (not StrictHostKeyChecking):
            ssh_args.extend(['-o', 'StrictHostKeyChecking=no'])

        if (self.known_hosts_path):
            ssh_args.extend(['-o',
                             'UserKnownHostsFile="{0}"'.format(
                                 self.known_hosts_path)])

        ssh_args.append('{0}@{1}'.format(self.username, self.address))

        return subprocess.run(ssh_args, check=check)

    def open_shell(self, StrictHostKeyChecking=False):

        ssh_args = ['ssh']

        if (self.key_path):
            ssh_args.extend(['-i', self.key_path])

        if (not StrictHostKeyChecking):
            ssh_args.extend(['-o', 'StrictHostKeyChecking=no'])

        if (self.known_hosts_path):
            ssh_args.extend(['-o',
                             'UserKnownHostsFile="{0}"'.format(
                                 self.known_hosts_path)])

        ssh_args.append(self.username + '@' + self.address)

        return subprocess.run(ssh_args)
