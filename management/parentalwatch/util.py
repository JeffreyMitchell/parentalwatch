# Copyright (C) 2019 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import json
import time
import calendar
from copy import copy, deepcopy

from parentalwatch import html_blocks

base_path = os.path.dirname(os.path.abspath(__file__))

def time_string(epoch):
    localtime = time.localtime(epoch)
    return '{} {},\t{:02d}:{:02d}:{:02d}, {}'.format(
        calendar.month_abbr[localtime.tm_mon],
        localtime.tm_mday,
        localtime.tm_hour,
        localtime.tm_min,
        localtime.tm_sec,
        localtime.tm_year)

def load_dstdom_acl(acl_path):

    hosts = set()
    with open(acl_path, 'r') as f:
        for line in f:
            line = line.strip()

            if (len(line) == 0 or line[0] == '#'):
                # This is a comment or empty line. Ignore.
                continue

            if (line.startswith('(^|\.)')):
                # This is a regex. Support for loading these is very minimal
                line = line.replace('\.', '.')[5:]

            if (line[-1] == '$'):
                line = line[:-1]

            if (line[0] != '.'):
                line = '.' + line

            hosts.add(line)

    return hosts

def load_filters(file_path):

    with open(file_path, 'r') as f:
        filters = json.load(f)

    hosts = filters['hosts']
    filters['hosts'] = set()

    for host in hosts:
        if (type(host) == str):
            if (host[0] != '.'):
                filters['hosts'].add('.' + host)
            else:
                filters['hosts'].add(host)

        elif (type(host) == dict and host['type'] == 'acl'):
            if (os.path.isabs(host['path'])):
                acl_path = host['path']
            else:
                acl_path = os.path.join(os.path.dirname(file_path), host['path'])

            acl_hosts = load_dstdom_acl(acl_path)

            filters['hosts'].update(acl_hosts)

    pages = filters['pages']
    filters['pages'] = set()

    for page in pages:
        if (type(page) == str):
            if (page[-1] != '/'):
                filters['pages'].add(page + '/')
            else:
                filters['pages'].add(page)

    # These filters get performance boosts from being converted to sets
    filters['status_codes'] = set(filters['status_codes'])
    filters['types'] = set(filters['types'])

    # Part of this type of filter gets a performance boost from using a set,
    # while the other requires a tuple. Creating both here is the fastest
    # solution.
    filters['tuple_hosts'] = tuple(filters['hosts'])
    filters['tuple_pages'] = tuple(filters['pages'])

    return filters

def load_default_filters():
    return load_filters('{0}/default_filters/filters.json'.format(base_path))

def join_url_dicts(dicts):

    if (len(dicts) == 0):
        return {}

    urls = deepcopy(dicts[0])

    for i in range(1, len(dicts)):

        for host, paths in dicts[i].items():

            if (host not in urls):
                urls[host] = paths
                continue

            for path, metadata in paths.items():

                if (path not in urls[host]):
                    urls[host][path] = metadata

                if (max(metadata['timestamps']) > \
                    max(urls[host][path]['timestamps'])):

                    urls[host][path]['protocol'] = metadata['protocol']
                    urls[host][path]['type'] = metadata['type']
                    urls[host][path]['http_status'] = metadata['http_status']

                for timestamp in metadata['timestamps']:
                    if (timestamp not in urls[host][path]['timestamps']):
                        urls[host][path]['timestamps'].append(timestamp)

    return urls

def split_url_dict(urls, callback, modify_original=True):

    new_urls = {}

    for host in list(urls.keys()):

        for path in list(urls[host].keys()):

            if (callback(urls[host][path])):
                if (host not in new_urls):
                    new_urls[host] = {}

                new_urls[host][path] = urls[host][path]
                if (modify_original):
                    del urls[host][path]

        if (len(urls[host].keys()) == 0):
            del urls[host]

    return new_urls

def write_html_url_dict(history_page, urls):

    host_text = html_blocks.get('host')
    details_text = html_blocks.get('details')
    image_text = html_blocks.get('image')
    video_text = html_blocks.get('video')
    audio_text = html_blocks.get('audio')

    # Sort by most recent timestamp in the host's paths' metadata
    for host, paths in sorted(
            urls.items(),
            key=lambda x: max([max(y['timestamps'])
                               for y in x[1].values()]),
            reverse=True):

        history_page.write(host_text.replace('$HOST', host))

        # Sort by the most recent timestamp in the metadata
        for path, metadata in sorted(
                paths.items(),
                key=lambda x: max(x[1]['timestamps']),
                reverse=True):

            full_link = '{0}://{1}{2}'.format(
                metadata['protocol'],
                host, path)
            cropped_link = full_link

            text = details_text

            if (metadata['type'].startswith('image')):
                cropped_link = '(image) ' + cropped_link
                text = text + image_text
            elif (metadata['type'].startswith('video')):
                cropped_link = '(video) ' + cropped_link
                text = text + video_text
            elif (metadata['type'].startswith('audio')):
                cropped_link = '(audio) ' + cropped_link
                text = text + audio_text

            if (len(cropped_link) > 100):
                cropped_link = cropped_link[:97] + '...'

            text = text.replace('$LINK', full_link)
            text = text.replace('$CROPPED_LINK', cropped_link)
            history_page.write(text)

            for timestamp in sorted(metadata['timestamps'],
                                    reverse=True):
                time_text = time_string(timestamp)
                history_page.write(
                    '    {}<br>\n'.format(
                        time_text.replace('\t', '&emsp;')))

            history_page.write('  </details>\n')

        history_page.write('</details>\n')
