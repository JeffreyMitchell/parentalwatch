#!/usr/bin/python3

# Copyright (C) 2019 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import sys
import shutil
import pathlib
import time
import subprocess
import json
import argparse
import ipaddress
import inspect
import webbrowser
from copy import copy, deepcopy

# Print to stderr
def printerr(string):
    print(string, file=sys.stderr)

try:
    from urllib3.util import parse_url
    from distutils.util import strtobool
    from dateutil.parser import parse as parse_date
except ImportError as e:
    reqs_path = os.path.join(os.path.dirname(__file__), 'requirements.txt')
    if (os.path.isfile(reqs_path)):
        printerr(('Error: Could not import required module'
                  " '{0}'.").format(e.name))
        printerr(('Please install requirements. (pip3 install -r'
                  ' "{0}")').format(reqs_path))
        sys.exit(79)
    else:
        raise e

from parentalwatch.ssh_tool import ssh_tool
from parentalwatch import Profile
from parentalwatch.util import load_default_filters, time_string

application_name = 'ParentalWatch'
version = 0.4
version_str = 'v0.4'

def print_get_log_results(good_devices, bad_devices):

    if (good_devices):
        print()
        print('Successfully retrieved logs from {}.'.format(
            ', '.join(good_devices)))
    else:
        printerr('Warning: Could not get logs from any devices.')
        return

    if (bad_devices):
        print()
        print('Could not get logs from {}.'.format(', '.join(bad_devices)))

def print_update_results(good_devices, bad_devices):

    if (good_devices):
        print()
        print('Successfully updated {}.'.format(', '.join(good_devices)))
        print('Reboot them for changes to take effect.')
    else:
        printerr('Warning: Could not update any devices.')
        return

    if (bad_devices):
        print()
        print('Could not update {}.'.format(', '.join(bad_devices)))

def get_time_from_string(timestring):
    try:
        datetime = parse_date(timestring)
        epoch = datetime.timestamp()
    except ValueError:
        # parse_date() will raise an exception if it can't understand.
        printerr('Error: Could not interpret date ({0}).'.format(timestring))
        sys.exit(22)

    if (epoch > time.time()):
        printerr('Error: Specified date (' + datetime.ctime() + \
                 ') is in the future.')
        sys.exit(22)

    return epoch

def open_browser(url):

    success = webbrowser.open_new_tab(url)

    # xdg-open is used as a backup because in some circumstances it works
    # when the webbrowser module does not.
    if (not success and shutil.which('xdg-open')):
        xdg_process = subprocess.run(['xdg-open', url])
        success = not xdg_process.returncode

    return success
    

def parse_args():
    parser = argparse.ArgumentParser() # description=''

    group = parser.add_mutually_exclusive_group()

    group.add_argument(
        '--get_logs',
        action='store_true',
        help='Get logs from available devices')

    group.add_argument(
        '--clear_logs',
        type=str,
        metavar='DEVICE',
        help='Irreversibly delete logs from a device')

    group.add_argument(
        '--show_history',
        '--show_page_history',
        action='store_true',
        dest='show_page_history',
        help='Open a browser tab with combined non-image history')

    group.add_argument(
        '--show_media_history',
        action='store_true',
        help='Open a browser tab with combined image history')

    group.add_argument(
        '--ban_site',
        type=str,
        metavar='HOST',
        help='Ban a site')

    group.add_argument(
        '--list_banned_sites',
        action='store_true',
        help='Print out all banned sites')

    group.add_argument(
        '--unban_site',
        type=str,
        metavar='HOST',
        help='Unban a site')

    group.add_argument(
        '--add_device',
        nargs='?',
        const='not_a_json',
        type=str,
        metavar='DEVICE',
        help='Add a device')

    group.add_argument(
        '--list_devices',
        action='store_true',
        help='Print out all devices')

    group.add_argument(
        '--remove_device',
        type=str,
        metavar='DEVICE',
        help='Remove a device by index or name')

    group.add_argument(
        '--create_key',
        action='store_true',
        help='Create a new SSH key')

    group.add_argument(
        '--add_key',
        type=str,
        metavar='PATH',
        help='Path to new (or replacement) private SSH key')

    group.add_argument(
        '--remove_key',
        action='store_true',
        help="Remove the active profile's private SSH key")

    group.add_argument(
        '--update_devices',
        action='store_true',
        help="Update the profile's devices' configuration remotely")

    group.add_argument(
        '--shell',
        type=str,
        metavar='DEVICE',
        help='Open a terminal on the specified device')

    group.add_argument(
        '--set_profile',
        type=str,
        metavar='NAME',
        help='Set the active profile')

    parser.add_argument(
        '--version',
        action='store_true',
        help='Print current version')

    return parser.parse_args()

if (__name__ == '__main__'):

    args = parse_args()

    if (args.version):
        print('{} {}'.format(application_name, version_str))
        sys.exit(0)

    global base_path
    base_path = os.path.dirname(os.path.abspath(__file__))

    import_path = os.path.relpath(os.path.dirname(inspect.getfile(Profile)),
                                  base_path)

    if (len(pathlib.Path(import_path).parts) > 1 or import_path[0] == '..'):
        base_path = os.path.join(os.path.expanduser('~'), '.parentalwatch')

    profile_name = None
    curr_profile_path = os.path.join(base_path, 'current_profile.txt')
    if (args.set_profile):

        profile_name = args.set_profile.strip()

        if (not profile_name):
            printerr('Error: Profile name cannot be an empty string.')
            sys.exit(2)

        path = os.path.join(base_path, 'profiles', profile_name + '.json')
        if (not os.path.exists(path)):

            prompt = ('The profile {} does not exist. Do you want'
                      ' to create it? ').format(profile_name)
            if (not strtobool(input(prompt).strip())):
                sys.exit(2)

            admin_account = input("What is the admin account's name? ").strip()

            os.makedirs(os.path.dirname(path), exist_ok=True)

            new_profile = Profile(profile_name, admin_account)
            new_profile.save(path)

            print('New profile saved to ' + path)

        with open(curr_profile_path, 'w') as f:
            f.write(profile_name)

    elif (os.path.exists(curr_profile_path)):
        with open(curr_profile_path, 'r') as f:
            profile_name = f.read().strip()

    else:
        printerr(('Error: No profile set. Please set or create one with the'
                  ' "--set_profile <name>" argument.'))
        print('For usage help, run {0} -h'.format(sys.argv[0]))
        sys.exit(0)

    try:
        profile = Profile.load(
            os.path.join(base_path, 'profiles', profile_name + '.json'),
            base_path=base_path)
    except FileNotFoundError:
        printerr('Error: Specified profile ({}) does not exist.'.format(
            profile_name))
        sys.exit(2)

    if (args.get_logs):

        try:
            good_devices, bad_devices = profile.retrieve_logs()
        except AssertionError as e:
            printerr('Error: {}'.format(e.args[0]))
            sys.exit(61)

        print_get_log_results(good_devices, bad_devices)

    elif (args.clear_logs):

        device_query = args.clear_logs

        try:
            device = profile.get_device(device_query)
        except IndexError:
            printerr("Error: '{}' is not a device's name or index.".format(
                device_query))
            sys.exit(22)

        question = ("Are you sure you want to clear all logs on {0}?"
                    " This can't be undone. ").format(device['name'])
        if (not strtobool(input(question).strip())):
            sys.exit(0)

        question = 'Are you REALLY sure? '
        if (not strtobool(input(question).strip())):
            sys.exit(0)

        ssh = ssh_tool(profile.admin_account, device['address'],
                       profile.get_ssh_key(), base_path + '/ssh_known_hosts')

        script = (
            '#!/bin/bash\n'
            'set -e\n'
            'srm {0}\n'
            'touch {0}\n'
            'chmod 660 {0}\n'
            'chown :squid {0}\n'
            'sudo systemctl reload squid'
        ).format(profile.squid_log_path)
        ssh.ssh(script, interactive=True)

        log_path = os.path.join(base_path, 'latest_logs', profile.name,
                                device['name'] + '.log')
        if (os.path.exists(log_path)):
            os.remove(log_path)

        print()
        print('Success')

    elif (args.show_page_history):

        filters = load_default_filters()

        timestring_1 = input(
            '(Optional) What is the earliest date to show history from?'
            ' ').strip()
        if (timestring_1):
            filters['start_time'] = get_time_from_string(timestring_1)

        timestring_2 = input(
            '(Optional) What is the latest date to show history from?'
            ' ').strip()
        if (timestring_2):
            filters['end_time'] = get_time_from_string(timestring_2)

        # If the dates are backwards or simultaneous, tell the user.
        if (filters.get('end_time', time.time()) <= filters.get('start_time', 0)):
            printerr(('Error: The first date ({0}) does not take place before'
                      ' the second date ({1}).').format(datetime_1.ctime(),
                                                        datetime_2.ctime()))
            exit(22)

        history_path = '{}/history_page.html'.format(base_path)

        try:
            profile.compile_history(history_path, filters=filters,
                                    include_images=False)
        except AssertionError as e:
            printerr('Error: {}'.format(e.args[0]))
            sys.exit(61)

        print()
        print('Opening history page with default browser.')
        if (os.name == 'nt'):
            print(('Note: Neither Internet Explorer nor Microsoft Edge will'
                   ' display properly.'))
        if (not open_browser(history_path)):
            printerr('Error while trying to open history page.')
            sys.exit(65)

    elif (args.show_media_history):

        filters = load_default_filters()

        timestring_1 = input(
            '(Optional) What is the earliest date to show history from?'
            ' ').strip()
        if (timestring_1):
            filters['start_time'] = get_time_from_string(timestring_1)

        timestring_2 = input(
            '(Optional) What is the latest date to show history from?'
            ' ').strip()
        if (timestring_2):
            filters['end_time'] = get_time_from_string(timestring_2)

        # If the dates are backwards or simultaneous, tell the user.
        if (filters.get('end_time', time.time()) <= filters.get('start_time', 0)):
            printerr(('Error: The first date ({0}) does not take place before'
                      ' the second date ({1}).').format(datetime_1.ctime(),
                                                        datetime_2.ctime()))
            sys.exit(22)

        history_path = '{}/history_page.html'.format(base_path)

        try:
            profile.compile_media(history_path, filters=filters)
        except AssertionError as e:
            printerr('Error: {}'.format(e.args[0]))
            sys.exit(61)

        print()
        print('Opening history page with default browser.')
        if (os.name == 'nt'):
            print(('Note: Neither Internet Explorer nor Microsoft Edge will'
                   ' display properly.'))
        if (not open_browser(history_path)):
            printerr('Error while trying to open history page.')
            sys.exit(65)

    elif (args.ban_site):

        host = parse_url(args.ban_site).host.lower()

        print('Banning site: ' + host)

        if (host in profile.banned_sites):
            print("'{}' is already banned.".format(host))
            sys.exit(22)

        for banned_site in profile.banned_sites:

            test_b_site = banned_site
            if (test_b_site != '.'):
                test_b_site = '.' + test_b_site

            if (('.' + host).endswith(test_b_site)
                or test_b_site.endswith('.' + host)):
                printerr(("Error: '{}' would conflict with the existing"
                          " entry '{}'").format(host, banned_site))
                sys.exit(22)

        profile.banned_sites.append(host)

        if (profile.devices):
            good_devices, bad_devices = profile.update_remote_configs()

            print_update_results(good_devices, bad_devices)

    elif (args.list_banned_sites):

        template = '{0:>%d}\t{1}' % (
            # This gets the largest number of digits an ID will have.
            len(str(len(profile.banned_sites) - 1)))

        print(template.format('Index', 'URL'))
        for i, site in enumerate(profile.banned_sites):
            print(template.format(i, site))

    elif (args.unban_site):

        try:
            host = profile.banned_sites[int(args.unban_site)]
        except ValueError:
            host = parse_url(args.unban_site).host.lower()

        print('Unbanning site: ' + host)

        if (host not in profile.banned_sites):
            print("'{}' is not already banned.".format(host))
            sys.exit(22)
        else:
            profile.banned_sites.remove(host)

        if (profile.devices):
            good_devices, bad_devices = profile.update_remote_configs()

            print_update_results(good_devices, bad_devices)

    elif (args.add_device):

        try:
            new_device = json.loads(args.add_device)
            new_device['last_updated'] = 0
        except json.decoder.JSONDecodeError:
            new_device = {
                'name': input("New device's name: ").strip(),
                'address': input("New device's address: ").strip(),
                'last_updated': 0
            }

        addr = new_device['address']

        if (addr != 'localhost'):
            try:
                ipaddress.ip_address(addr)
            except ValueError:
                printerr(("Warning: '{}' is not a valid IP address."
                          '').format(addr))

        if (new_device['name'] in [d['name'] for d in profile.devices]):
            printerr("Error: '{}' is already a device's name".format(
                new_device['name']))
            sys.exit(22)
        elif (addr in [d['address'] for d in profile.devices]):
            printerr("Error: '{}' is already a device's address.".format(addr))
            sys.exit(22)
        else:
            profile.devices.append(new_device)

        ssh = ssh_tool(profile.admin_account, addr, profile.get_ssh_key(),
                       '{0}/ssh_known_hosts'.format(base_path))

        if (ssh.ssh('exit 0', check=False).returncode):
            printerr("Error: Couldn't connect to the new device.")
            sys.exit(22)

        profile.update_remote_configs()

        print()
        print('Success')

    elif (args.list_devices):

        max_name_len = 0
        max_addr_len = 0

        if (profile.devices):
            # These get the lengths of the longest name and address
            max_name_len = max([len(dev['name']) for dev in profile.devices])
            max_addr_len = max([len(dev['address']) for dev in profile.devices])

        template = '{0:>%d}\t{1:<%d}\t{2:<%d}\t{3}' % (
            # This gets the largest number of digits an ID will have.
            len(str(len(profile.devices) - 1)),
            max_name_len, max_addr_len)

        print(template.format('Index', 'Name', 'Address', 'Last Updated'))

        for i, dev in enumerate(profile.devices):
            print(template.format(i, dev['name'], dev['address'],
                                  time_string(dev['last_updated'])))

    elif (args.remove_device):

        device_query = args.remove_device

        try:
            device = profile.get_device(device_query)
        except IndexError:
            printerr("Error: '{}' is not a device's name or index.".format(
                device_query))
            sys.exit(22)

        profile.devices.remove(device)

        # Removing the key is not entirely necessary, but it is good practice.
        if (profile.get_ssh_key()):
            profile.remove_key_remotely(ip_addresses=[device['address']])

        subprocess.run(['ssh-keygen', '-R', device['address'], '-f',
                        '{0}/ssh_known_hosts'.format(base_path)])

    elif (args.create_key):

        key_path = profile.get_ssh_key()
        if (key_path):
            printerr('Error: Key already exists.')
            sys.exit(17)

        key_path = '{0}/profiles/{1}.pem'.format(base_path, profile.name)

        subprocess.run(['ssh-keygen', '-N', '', '-f', key_path], check=True)

        profile.update_remote_configs()

        print()
        print('Success')

    elif (args.add_key):

        key_path = profile.get_ssh_key()
        if (key_path):
            printerr('Error: Key already exists.')
            sys.exit(17)

        key_path = '{0}/profiles/{1}.pem'.format(base_path, profile.name)

        shutil.copy(args.add_key, key_path)

        profile.update_remote_configs()

        print()
        print('Success')

    elif (args.remove_key):

        profile.remove_key_remotely()
        os.remove(profile.get_ssh_key())

        print()
        print('Success')

    elif (args.update_devices):

        if (not profile.devices):
            printerr('Error: No devices to update.')
            sys.exit(22)

        good_devices, bad_devices = profile.update_remote_configs()

        print_update_results(good_devices, bad_devices)

    elif (args.shell):

        device_query = args.shell

        try:
            device = profile.get_device(device_query)
        except IndexError:
            printerr("Error: '{}' is not a device's name or index.".format(
                device_query))
            sys.exit(22)

        ssh = ssh_tool(profile.admin_account, device['address'],
                       profile.get_ssh_key(),
                       '{0}/ssh_known_hosts'.format(base_path))
        sys.exit(ssh.open_shell().returncode)

    elif (not args.set_profile):
        print('For usage help, run {0} -h'.format(sys.argv[0]))

    profile.save()
