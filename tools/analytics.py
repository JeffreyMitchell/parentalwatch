#!/usr/bin/python3

# Copyright (C) 2019 Jeffrey Mitchell

# This file is part of ParentalWatch.
# ParentalWatch is distributed under GPLv2 license.

import os
import sys
import pathlib
import json
import argparse
import inspect

from parentalwatch import Profile
from parentalwatch.util import load_default_filters

# Print to stderr
def printerr(string):
    print(string, file=sys.stderr)

def perform_analytics(urls):

    #hosts = {}
    return_types = {}
    return_codes = {}

    for host, paths in urls.items():
        #hosts[host] = hosts.get(host, 0) + 1
        for path, meta in paths.items():
            return_types[meta['type']] = return_types.get(meta['type'], 0) + 1
            return_codes[meta['http_status']] = return_codes.get(meta['http_status'], 0) + 1

    #print('Hosts: ' + json.dumps(hosts, indent=4, sort_keys=True))
    print('Return types: ' + json.dumps(return_types, indent=4, sort_keys=True))
    print('HTTP codes: ' + json.dumps(return_codes, indent=4))

def parse_args():
    parser = argparse.ArgumentParser() # description=''

    group = parser.add_mutually_exclusive_group()

    group.add_argument(
        '--type',
        type=str,
        help='')

    group.add_argument(
        '--http_code',
        type=int,
        help='')

    group.add_argument(
        '--host',
        type=str,
        help='')

    parser.add_argument(
        '--filtered',
        action='store_true',
        help='')

    return parser.parse_args()

if (__name__ == '__main__'):

    args = parse_args()

    global base_path
    base_path = os.path.dirname(os.path.abspath(__file__))

    import_path = os.path.relpath(os.path.dirname(inspect.getfile(Profile)),
                                  base_path)

    if (len(pathlib.Path(import_path).parts) > 1 or import_path[0] == '..'):
        base_path = os.path.join(os.path.expanduser('~'), '.parentalwatch')

    curr_profile_path = os.path.join(base_path, 'current_profile.txt')
    if (os.path.exists(curr_profile_path)):
        with open(curr_profile_path, 'r') as f:
            profile_name = f.read().strip()

    else:
        printerr(('Error: No profile set. Please set or create one with the'
                  ' CLI.'))
        sys.exit(0)

    try:
        profile = Profile.load(
            '{0}/profiles/{1}.json'.format(base_path, profile_name),
            base_path=base_path)
    except FileNotFoundError:
        print('Specified profile ({}) does not exist.'.format(profile_name))
        sys.exit(2)

    filters = None

    if (args.filtered):
        filters = load_default_filters()

    urls = profile.get_history(filters)[0]

    if (args.type):

        full_urls = []

        for host, paths in urls.items():
            for path, meta in paths.items():
                if (meta['type'] == args.type):
                    full_urls.append({
                        'url': '{0}://{1}{2}'.format(
                            meta['protocol'], host, path),
                        'http_code': meta['http_status'],
                        'size': meta['size']
                    })

        print(json.dumps(full_urls, indent=4, sort_keys=True))

    elif (args.http_code):

        full_urls = []

        for host, paths in urls.items():
            for path, meta in paths.items():
                if (meta['http_status'] == args.http_code):
                    full_urls.append({
                        'url': '{0}://{1}{2}'.format(
                            meta['protocol'], host, path),
                        'type': meta['type'],
                        'size': meta['size']
                    })

        print(json.dumps(full_urls, indent=4, sort_keys=True))

    elif (args.host):

        full_urls = [{
            'url': '{0}://{1}{2}'.format(meta['protocol'], args.host, path),
            'type': meta['type'],
            'http_code': meta['http_status'],
            'size': meta['size']
        }
                     for path, meta in urls[args.host].items()]

        print(json.dumps(full_urls, indent=4, sort_keys=True))

    else:
        perform_analytics(urls)
