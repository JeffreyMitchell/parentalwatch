# ParentalWatch

ParentalWatch fills the gap on Linux between simple, breakable parental controls and complex, highly effective parental control systems. It does this by automating the process of installing and maintaining a transparent proxy, along with other tools, on the child's computer. ParentalWatch primarily serves to unobtrusively monitor web traffic, but also blocks certain known-malicious websites based on the hostname and allows parents to remotely block additional websites of their choice.

It is important to note that while ParentalWatch allows remote management of a child's devices, it uses no central server. Using ParentalWatch does not expose your child's activity logs to any company's database. However, without a central server, management must be done from the same local network as the child's device, and its IP address will very likely change from time to time.

ParentalWatch is split into two different pieces:
    The setup script, on the child's computer, which installs and configures dependencies, proxy, DNS, firewall, and automatic system updates.
    The management program on the parent's computer, which serves to transfer logs, filter them for organized viewing, and remotely make configuration changes to block additional sites.

## Compatibility

ParentalWatch's child-side setup script currently only works on Debian-based Linux distros, but would not take much tweaking to work on other Linux systems too.
The following child-side Linux 64-bit distros have been tested:  
- Linux Mint 20  
- Linux Mint 19  
- Ubuntu 20.04  
- Ubuntu 18.04  
- Debian 10  

ParentalWatch's remote management script should work on any device after you install Python3, Pip (A python dependency manager), and SSH. The following Operating Systems are known to work:  
- Any version of Linux  
- Windows 10  
- Android (Tested through the app Termux. There are some caveats that make it less than desirable.)  
- macOS (Untested, but there's no good reason for it not to work.)  

The management script will NOT run on Windows 8.1 or earlier. This is because Microsoft has not added an SSH client to those versions. It is technically possible if you can add an SSH client to your PATH variable, but at that point, it's less effort to setup a Linux VM.

## Installation

NOTE: The child should not have root access on their computer. If they do, they will have everything they need to completely dismantle the monitoring and controls.

### Child-side

You need administrator permissions, and should ensure that the child does not have such access.  
Download and extract the ParentalWatch repo into your Downloads folder.  
Open a terminal and navigate to `parentalwatch/device_setup/`.
    This will usually be done with the command `cd Downloads/parentalwatch/device_setup/`.  
Run the command `sudo ./setup.py -y` and input your password if needed.  
    The setup script will go through all of its tasks and inform you if it succeeds.  
Reboot for changes to take effect.

### Parent-side

Install Python 3, Pip, and SSH.  
Download and extract the ParentalWatch repo into your Home folder.  
Open a terminal and navigate to `parentalwatch/management/`.
    This will usually be done with the command `cd parentalwatch/management/`  
Install required python modules with the command `pip3 install -r requirements.txt`  
On Windows, remove the `./` from the beginning of `cli.py` commands.  
Run `./cli.py --set_profile <profile_name>` with the name you want to describe your local profile (Maybe your child's name)  
    Answer "yes" when you are asked if you want to create a new profile.  
    Type in your username on the child's computer. This is used for remote-access.  
Run `./cli.py --create_key` to generate an SSH key to reduce the number of times you have to input the password to your account on the child's computer.  

ParentalWatch can be used to manage multiple devices. Register a device (Which needs to be on) with the command `./cli.py --add_device`. You will be prompted for an identifying name and the device's IP address for remote management. List all devices with `./cli.py --list_devices` and remove one by substituting the device's ID into `./cli.py --remove_device <device_id>`. IP addresses will change between different networks. To update an IP address, simply remove the device and re-add it.

## Guide

Here are some common `cli.py` commands to run:  
- `./cli.py --help` Prints out a list of all available arguments  
- `./cli.py --get_logs` Attempts to connect to each configured device and copy history logs from them. Do this before viewing the history in order to have up-to-date information.  
- `./cli.py --show_history` Generates an HTML page containing a concise representation of all of the available log files. Opens the default web browser to display it. (Neither MS Edge nor Internet Explorer correctly render.)  
- `./cli.py --ban_site <URL>` Attempts to connect to each configured device and add config options to block the specified website  
- `./cli.py --list_banned_sites` Prints a list of banned sites  
- `./cli.py --unban_site` Unbans a site banned with ` --ban_site`  
- `./cli.py --update_devices` Attempts to connect to each configured device and update their configuration (SSH key, banned sites)  
- `./cli.py --shell <device_id>` Opens a shell in the specified device  
You are all set. For a help menu, run `./cli.py --help`

## Open-Source License

Copyright (C) 2019-2021 Jeffrey Mitchell  
ParentalWatch is distributed under GPLv2 license.

## [Design Overview](docs/design_overview.odt)
